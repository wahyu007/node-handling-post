const http = require('http');
const controller = require('./controllers/app_controller');
http.createServer(controller).listen(3000, () => {
    console.log("listen on port 3000");
});